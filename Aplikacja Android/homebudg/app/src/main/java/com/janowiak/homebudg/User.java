package com.janowiak.homebudg;


import java.util.ArrayList;

public class User {
    private String id;
    private ArrayList categoryList;
    public String getID() {return id;}
    public void setID(String id) {this.id = id;}
    private static final User user = new User();
    public static User getInstance() {return user;}
    public void setCategory(ArrayList list){ this.categoryList = list; }
    public ArrayList getCategory() {return categoryList;}
}
