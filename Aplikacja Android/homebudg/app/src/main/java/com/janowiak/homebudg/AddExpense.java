package com.janowiak.homebudg;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class AddExpense extends Activity{
    EditText date;
    EditText amount;
    EditText note;
    Spinner category;
    Spinner subCategory;
    Spinner payee;
    Button addExpense;
    String insertUrl = "http://192.168.0.8:80/HomeBudget/new_expense.php";
    RequestQueue requestQueue;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.add_expense_activity);
        Log.d("addexpens", "hej");
        date = (EditText) findViewById(R.id.date);
        amount = (EditText) findViewById(R.id.amount);
        note = (EditText) findViewById(R.id.note);
        category = (Spinner) findViewById(R.id.category_spin);
        subCategory = (Spinner) findViewById(R.id.sub_category_spin);
        payee = (Spinner) findViewById(R.id.payee_spin);
        addExpense = (Button) findViewById(R.id.add_expense);

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        addExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringRequest request = new StringRequest(Request.Method.POST, insertUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> parameters = new HashMap<String, String>();
                        parameters.put("user_id", User.getInstance().getID());
                        parameters.put("date", date.getText().toString());
                        parameters.put("amount", amount.getText().toString());
                        parameters.put("note", note.getText().toString());
                        parameters.put("sub_category_id", subCategory.getSelectedItem().toString());
                        parameters.put("payee_id", payee.getSelectedItem().toString());
                        return parameters;
                    }
                };
                requestQueue.add(request);

                startActivity(new Intent(getApplicationContext(), ExpensesList.class));
            }
        });
    }
}
