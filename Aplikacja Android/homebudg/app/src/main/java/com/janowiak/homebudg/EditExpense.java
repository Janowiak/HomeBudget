package com.janowiak.homebudg;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.janowiak.homebudg.ExpenseListScreen.ExpensesList;
import com.janowiak.homebudg.NewExpenseScreen.Payee;
import com.janowiak.homebudg.NewExpenseScreen.SubCategory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class EditExpense extends Activity {

    EditText date;
    EditText amount;
    EditText note;
    Spinner category;
    Spinner subCategory;
    Spinner payee;
    Button updateExpense;
    Button deleteExpense;
    String updateUrl = Config.getInstance().getIP() +"HomeBudget/update_or_delete.php";
    RequestQueue requestQueue;
    ArrayAdapter adapterSubCat;
    ArrayAdapter adapterPayee;
    ArrayList fullSubCategory;
    SubCategory.SubCat chosenSubCat;
    Payee.PayeeFull chosenPayee;
    ArrayList fullPayee;

    void changeSubCat(int position){
        SubCategory subCat = new SubCategory(getApplicationContext(), category.getSelectedItem().toString());
        fullSubCategory = subCat.getCatAndSubId();
        ArrayList<String> newSubCat = subCat.getList();
        adapterSubCat = new ArrayAdapter(this, android.R.layout.simple_spinner_item, newSubCat);
        adapterSubCat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subCategory.setAdapter(adapterSubCat);
        subCategory.postDelayed(new Runnable() {
            public void run() {
                adapterSubCat.notifyDataSetChanged();
                subCategory.setSelection(1);
            }
        }, 100);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_expense);
        date = (EditText) findViewById(R.id.date);
        amount = (EditText) findViewById(R.id.amount);
        note = (EditText) findViewById(R.id.note);
        category = (Spinner) findViewById(R.id.category_spin);
        subCategory = (Spinner) findViewById(R.id.sub_category_spin);
        payee = (Spinner) findViewById(R.id.payee_spin);
        updateExpense = (Button) findViewById(R.id.update_expense);
        deleteExpense = (Button) findViewById(R.id.delete_expense);

        ArrayList<String> categoryList = User.getInstance().getCategory();
        ArrayAdapter adapterCat = new ArrayAdapter(this, android.R.layout.simple_spinner_item, categoryList);
        adapterCat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category.setAdapter(adapterCat);

        ArrayList<String> subCategoryList = new SubCategory(getApplicationContext(), category.getSelectedItem().toString()).getList();
        adapterSubCat = new ArrayAdapter(this, android.R.layout.simple_spinner_item, subCategoryList);
        adapterSubCat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subCategory.setAdapter(adapterSubCat);

        Payee payeeObject = new Payee(getApplicationContext());
        ArrayList<String> payeeList = payeeObject.getList();
        fullPayee = payeeObject.getPayee();
        adapterPayee = new ArrayAdapter(this, android.R.layout.simple_spinner_item, payeeList);
        adapterSubCat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        payee.setAdapter(adapterPayee);

        date.setText(ItemToEdit.getInstance().getItem().date);
        amount.setText(ItemToEdit.getInstance().getItem().amount);
        note.setText(ItemToEdit.getInstance().getItem().notes);
        category = (Spinner) findViewById(R.id.category_spin);
        subCategory = (Spinner) findViewById(R.id.sub_category_spin);

        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                changeSubCat(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        subCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
                payee.postDelayed(new Runnable() {
                    public void run() {
                        adapterPayee.notifyDataSetChanged();
                        payee.setSelection(1);
                    }
                }, 100);
                chosenSubCat = (SubCategory.SubCat)fullSubCategory.get(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        payee.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                chosenPayee = (Payee.PayeeFull) fullPayee.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        updateExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringRequest request = new StringRequest(Request.Method.POST, updateUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> parameters = new HashMap<String, String>();
                        parameters.put("id", ItemToEdit.getInstance().getItem().id);
                        parameters.put("user_id", User.getInstance().getID());
                        parameters.put("date", date.getText().toString());
                        parameters.put("amount", amount.getText().toString());
                        parameters.put("notes", note.getText().toString());
                        parameters.put("sub_category_id", chosenSubCat.id_subCat);
                        parameters.put("payee_id", chosenPayee.id);
                        return parameters;
                    }
                };
                requestQueue.add(request);
                startActivity(new Intent(getApplicationContext(), ExpensesList.class));
            }
        });

        deleteExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringRequest request = new StringRequest(Request.Method.POST, updateUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> parameters = new HashMap<String, String>();
                        parameters.put("id", ItemToEdit.getInstance().getItem().id);
                        return parameters;
                    }
                };
                requestQueue.add(request);
                startActivity(new Intent(getApplicationContext(), ExpensesList.class));
            }
        });
    }
}
