package com.janowiak.homebudg;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ExpensesList extends Activity{

    Button addExpense;
    String showUrl = "http://192.168.0.8:80/HomeBudget/show_expense.php";
    ListView expensesList;
    RequestQueue requestQueue;
    ArrayAdapter<String> adapter;
    ArrayList<String> result = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expenses_list);
        expensesList = (ListView) findViewById(R.id.expensesList);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, result);
        expensesList.setAdapter(adapter);

        addExpense = (Button) findViewById(R.id.add_expense);

        addExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AddExpense.class));
            }
        });

        requestQueue = Volley.newRequestQueue(getApplicationContext());
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("id", User.getInstance().getID());
        CustomRequest jsonObjectRequest = new CustomRequest(Request.Method.POST, showUrl, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray expenses = response.getJSONArray("expenses");
                    for(int i=0; i < expenses.length(); i++){
                        JSONObject expense = expenses.getJSONObject(i);
                        String notes = expense.getString("notes");
                        String amount = expense.getString("amount");
                        String subCategory = expense.getString("sub_category_id");
                        result.add(notes + " " + amount + "zł");
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        adapter.notifyDataSetChanged();
        requestQueue.add(jsonObjectRequest);
        adapter.notifyDataSetChanged();
    }
}
