package com.janowiak.homebudg.NewExpenseScreen;

import android.content.Context;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.janowiak.homebudg.Config;
import com.janowiak.homebudg.CustomRequest;
import com.janowiak.homebudg.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;

public class Payee {
    ArrayList<String> result = new ArrayList<>();
    String showUrl = Config.getInstance().getIP() +"HomeBudget/show_payee.php";
    RequestQueue requestQueue;
    ArrayList<PayeeFull> payeeList = new ArrayList<>();

    public class PayeeFull{
        public String id;
        String name;
        public PayeeFull(String id, String name){
            this.id = id;
            this.name = name;
        }
    }

    public Payee(final Context context){
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("id", User.getInstance().getID());


        CustomRequest jsonObjectRequest = new CustomRequest(Request.Method.POST, showUrl, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray categories = response.getJSONArray("payee");
                    for(int i=0; i < categories.length(); i++){
                        JSONObject category = categories.getJSONObject(i);
                        String categoryName = category.getString("name");
                        result.add(categoryName);
                        payeeList.add(new PayeeFull(category.getString("id"), category.getString("name")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public ArrayList getList(){
        return result;
    }

    public ArrayList getPayee(){
        return payeeList;
    }
}
