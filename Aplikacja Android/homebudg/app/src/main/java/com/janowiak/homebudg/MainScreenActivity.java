package com.janowiak.homebudg;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.janowiak.homebudg.ExpenseListScreen.ExpensesList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;

public class MainScreenActivity extends Activity {

    private Button log_out;
    private Button expenses;
    private Button incomes;
    TextView lastExpense;
    TextView expenseSum;
    TextView monthExpenseSum;
    Spinner months;
    TextView incomeSum;
    TextView balance;
    String getSumUrl = Config.getInstance().getIP() + "HomeBudget/get_sum.php";
    String getMonthExpenseUrl = Config.getInstance().getIP() + "HomeBudget/get_month_expense.php";
    RequestQueue requestQueue;
    private String[] monthArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        log_out = (Button) findViewById(R.id.log_out);
        expenses = (Button) findViewById(R.id.expenses);
        incomes = (Button) findViewById(R.id.incomes);
        lastExpense = (TextView) findViewById(R.id.text_last_expense);
        expenseSum = (TextView) findViewById(R.id.text_expense_sum);
        monthExpenseSum = (TextView) findViewById(R.id.text_month_expense);
        months = (Spinner) findViewById(R.id.spinner_month);
        incomeSum = (TextView) findViewById(R.id.text_income_sum);
        balance = (TextView) findViewById(R.id.text_balance);

        this.monthArray = new String[] {
                "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"
        };
        months = (Spinner) findViewById(R.id.spinner_month);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, monthArray);
        months.setAdapter(adapter);

        months.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getMonthSum(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });

        expenses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ExpensesList.class));
            }
        });

        incomes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });


        requestQueue = Volley.newRequestQueue(getApplicationContext());
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("id", User.getInstance().getID());
        CustomRequest jsonObjectRequest = new CustomRequest(Request.Method.POST, getSumUrl, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray expensesInfo = response.getJSONArray("expensesInfo");
                    JSONObject expenseSumInfo = expensesInfo.getJSONObject(0);
                    expenseSum.setText(expenseSumInfo.getString("FullExpenseAmount") + " zł");
                    expenseSumInfo = expensesInfo.getJSONObject(1);
                    incomeSum.setText(expenseSumInfo.getString("FullIncomeAmount") + " zł");
                    expenseSumInfo = expensesInfo.getJSONObject(2);
                    lastExpense.setText(expenseSumInfo.getString("last"));
                    expenseSumInfo = expensesInfo.getJSONObject(3);
                    balance.setText(expenseSumInfo.getString("Balance"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public void getMonthSum(int month){
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("id", User.getInstance().getID());
        hashMap.put("month", ""+(month+1));
        CustomRequest jsonObjectRequest = new CustomRequest(Request.Method.POST, getMonthExpenseUrl, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray monthExpense = response.getJSONArray("monthExpense");
                    JSONObject monthExp = monthExpense.getJSONObject(0);
                    String sumMonth = monthExp.getString("MonthFullExpense");
                    if(sumMonth == "null") monthExpenseSum.setText("0 zł");
                    else monthExpenseSum.setText(monthExp.getString("MonthFullExpense") + " zł");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        requestQueue.add(jsonObjectRequest);
    }
}