package com.janowiak.homebudg.unused;


import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.janowiak.homebudg.CustomRequest;
import com.janowiak.homebudg.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;

public class SubCategoryAdapter extends BaseAdapter{

    ArrayList<String> result = new ArrayList<>();
    String showUrl = "http://192.168.0.8:80/HomeBudget/show_sub_category.php";
    RequestQueue requestQueue;
    Context context;
    LayoutInflater inflater;


    public SubCategoryAdapter(Context context, String categoryName){
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("id", User.getInstance().getID());
        hashMap.put("name", categoryName);

        CustomRequest jsonObjectRequest = new CustomRequest(Request.Method.POST, showUrl, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray categories = response.getJSONArray("sub_category");
                    for(int i=0; i < categories.length(); i++){
                        JSONObject category = categories.getJSONObject(i);
                        String categoryName = category.getString("name");
                        result.add(categoryName);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public int getCount() {
        return result.size();
    }

    @Override
    public String getItem(int position) {
        return result.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_spinner_item, null);
        }
        TextView textView = (TextView) convertView.findViewById(android.R.id.text1);

        textView.setText(getItem(position));
        return convertView;
    }
}
