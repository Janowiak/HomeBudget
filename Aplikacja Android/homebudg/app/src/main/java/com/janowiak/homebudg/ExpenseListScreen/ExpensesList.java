package com.janowiak.homebudg.ExpenseListScreen;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.janowiak.homebudg.Config;
import com.janowiak.homebudg.CustomRequest;
import com.janowiak.homebudg.EditExpense;
import com.janowiak.homebudg.ItemToEdit;
import com.janowiak.homebudg.MainScreenActivity;
import com.janowiak.homebudg.NewExpenseScreen.NewExpense;
import com.janowiak.homebudg.R;
import com.janowiak.homebudg.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class ExpensesList extends Activity{

    Button addExpense;
    String showUrl = Config.getInstance().getIP() + "HomeBudget/show_expense.php";
    ListView expensesList;

    TextView counter;
    ArrayList<Expense> expenseList = new ArrayList<>();
    String countExpense = "Ilość zapisanych wydatków: 0";
    Button backToMain;
    Button search;
    EditText searchSentence;

    public class Expense {
        public String id, date, amount, notes, subCatID, subCatName, catID, catName;
        public Expense(String id, String date, String amount, String notes, String subCatID, String subCatName, String catID, String catName){
            this.id = id;
            this.date = date;
            this.amount = amount;
            this.notes = notes;
            this.subCatID = subCatID;
            this.subCatName = subCatName;
            this.catID = catID;
            this.catName = catName;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        expensesList.invalidateViews();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expenses_list);
        expensesList = (ListView) findViewById(R.id.expensesList);
        expensesList.setAdapter(new ExpenseListAdapter());
        counter = (TextView) findViewById(R.id.count_items);
        counter.setText(countExpense);
        addExpense = (Button) findViewById(R.id.add_expense);
        backToMain = (Button) findViewById(R.id.back_to_main);
        search = (Button) findViewById(R.id.search_expense);
        searchSentence = (EditText) findViewById(R.id.expense_text);
        setExpenseList("nosearch");

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expenseList = new ArrayList<>();
                setExpenseList(searchSentence.getText().toString());
                Toast.makeText(ExpensesList.this, searchSentence.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        backToMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainScreenActivity.class));
            }
        });

        addExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), NewExpense.class));
            }
        });


        expensesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ItemToEdit.getInstance().setItem((Expense) expensesList.getAdapter().getItem(position));

                startActivity(new Intent(getApplicationContext(), EditExpense.class));
            }
        });


    }

    void setExpenseList(String searchText){
        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("id", User.getInstance().getID());
        if(searchText != "nosearch"){
            hashMap.put("search", searchText);
        }
        CustomRequest jsonObjectRequest = new CustomRequest(Request.Method.POST, showUrl, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray expenses = response.getJSONArray("expenses");
                    for(int i=0; i < expenses.length(); i++){
                        JSONObject expense = expenses.getJSONObject(i);
                        if(i<expenses.length()-1) {
                            String id = expense.getString("id");
                            String date = expense.getString("date");
                            String notes = expense.getString("notes");
                            String amount = expense.getString("amount");
                            String subCatID = expense.getString("sub_category_id");
                            String subCatName = expense.getString("subcategory");
                            String catID = expense.getString("category_id");
                            String catName = expense.getString("category");
                            expenseList.add(new Expense(id, date, amount, notes, subCatID, subCatName, catID, catName));
                            expensesList.invalidateViews();
                        }else{
                            countExpense = "Ilość zapisanych wydatków: "+expense.getString("counter");
                            counter.setText(countExpense);
                            expensesList.invalidateViews();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        requestQueue.add(jsonObjectRequest);
        expensesList.invalidateViews();
    }


    public class ExpenseListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return expenseList.size();
        }

        @Override
        public ExpensesList.Expense getItem(int position) {
            return expenseList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return  position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.expense_list_item, null);
            }

            TextView title = (TextView) convertView.findViewById(R.id.title);
            TextView price = (TextView) convertView.findViewById(R.id.price);
            TextView sub_category = (TextView) convertView.findViewById(R.id.category);
            Expense item = getItem(position);
            title.setText(item.notes);
            price.setText(item.amount + " zł");
            sub_category.setText(item.subCatName);
            return convertView;
        }
    }
}
