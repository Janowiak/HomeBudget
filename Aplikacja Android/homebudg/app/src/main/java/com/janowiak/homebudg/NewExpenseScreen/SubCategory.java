package com.janowiak.homebudg.NewExpenseScreen;

import android.content.Context;
import android.view.LayoutInflater;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.janowiak.homebudg.Config;
import com.janowiak.homebudg.CustomRequest;
import com.janowiak.homebudg.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class SubCategory {

    ArrayList<String> result = new ArrayList<>();
    String showUrl = Config.getInstance().getIP() +"HomeBudget/show_sub_category.php";
    RequestQueue requestQueue;
    Context context;
    LayoutInflater inflater;

    public class SubCat{
        String id_cat;
        public String id_subCat;
        String name;
        private SubCat(String id_category, String id_subcategory, String name){
            this.id_cat = id_category;
            this.id_subCat = id_subcategory;
            this.name = name;
        }
    }

    ArrayList<SubCat> subCategories = new ArrayList<>();

    public SubCategory(Context context, String categoryName){
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("id", User.getInstance().getID());
        hashMap.put("name", categoryName);

        CustomRequest jsonObjectRequest = new CustomRequest(Request.Method.POST, showUrl, hashMap, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray categories = response.getJSONArray("sub_category");
                    for(int i=0; i < categories.length(); i++){
                        JSONObject category = categories.getJSONObject(i);
                        String categoryName = category.getString("name");
                        result.add(categoryName);
                        subCategories.add(i, new SubCat(category.getString("category_id"), category.getString("id"),category.getString("name")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public ArrayList getList(){
        return result;
    }

    public ArrayList getCatAndSubId(){
        return subCategories;
    }

}
