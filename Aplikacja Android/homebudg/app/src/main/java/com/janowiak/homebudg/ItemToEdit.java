package com.janowiak.homebudg;


import com.janowiak.homebudg.ExpenseListScreen.ExpensesList;

public class ItemToEdit {
    private ExpensesList.Expense item;
    private static final ItemToEdit itemToEdit = new ItemToEdit();
    public static ItemToEdit getInstance() {return itemToEdit;}
    public void setItem(ExpensesList.Expense item){ this.item = item; }
    public ExpensesList.Expense getItem() {return item;}
}
