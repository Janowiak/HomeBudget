<?php
if($_SERVER["REQUEST_METHOD"]=="POST") {
    require 'connect.php';
    $user_ID = $_POST['id'];
    if(isset($_POST['search'])){
        $search = $_POST['search'];
        searchExpense($user_ID, $search);
    }else showExpense($user_ID);
}

function showExpense($id){
    global $connect;
    $query = "SELECT expense.id, expense.date, expense.amount, expense.notes, expense.sub_category_id, sub_category.name AS 'subcategory', sub_category.category_id, category.name AS 'category'
              FROM expense
              INNER JOIN
		              sub_category
                  ON  expense.sub_category_id = sub_category.id
              INNER JOIN
		              category
                  ON sub_category.category_id = category.id
              WHERE expense.user_id = '$id';";
    $query2 = "SELECT COUNT(*) AS 'counter' FROM expense WHERE user_id='$id';";
    $counter = mysqli_query($connect, $query2);
    $result = mysqli_query($connect, $query);
    $number_of_rows = mysqli_num_rows($result);

    $temp_array = array();

    if($number_of_rows > 0){
        while ($row = mysqli_fetch_assoc($result)){
            $temp_array[] = $row;
        }
    }

    $temp_array[$number_of_rows] = mysqli_fetch_assoc($counter);

    header('Content-Type: application/json');
    echo json_encode(array("expenses"=>$temp_array));
}

function searchExpense($id, $searchText){
    global $connect;
    $query = "SELECT expense.id, expense.date, expense.amount, expense.notes, expense.sub_category_id, sub_category.name AS 'subcategory', sub_category.category_id, category.name AS 'category'
              FROM expense
              INNER JOIN
		              sub_category
                  ON  expense.sub_category_id = sub_category.id
              INNER JOIN
		              category
                  ON sub_category.category_id = category.id
              WHERE (expense.user_id = '$id' AND expense.notes LIKE '%$searchText%');";
    $query2 = "SELECT COUNT(*) AS 'counter' FROM expense WHERE user_id='$id' AND notes LIKE '%$searchText%';";
    $counter = mysqli_query($connect, $query2);
    $result = mysqli_query($connect, $query);
    $number_of_rows = mysqli_num_rows($result);

    $temp_array = array();

    if($number_of_rows > 0){
        while ($row = mysqli_fetch_assoc($result)){
            $temp_array[] = $row;
        }
    }

    $temp_array[$number_of_rows] = mysqli_fetch_assoc($counter);

    header('Content-Type: application/json');
    echo json_encode(array("expenses"=>$temp_array));
}