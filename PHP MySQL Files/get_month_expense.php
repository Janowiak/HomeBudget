<?php
if($_SERVER["REQUEST_METHOD"]=="POST") {
    require 'connect.php';
    $user_ID = $_POST['id'];
    $month = $_POST['month'];
    getMonthExpense($user_ID, $month);
}

function getMonthExpense($id, $month){
    global $connect;
    $callMonthExpenseQuery = "CALL GetMonthFullExpense('$month', '$id')";
    $monthExpenseResult = mysqli_query($connect, $callMonthExpenseQuery);

    $temp_array = array();
    $temp_array[0] = mysqli_fetch_assoc($monthExpenseResult);

    header('Content-Type: application/json');
    echo json_encode(array("monthExpense"=>$temp_array));
}