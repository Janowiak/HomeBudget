<?php
if($_SERVER["REQUEST_METHOD"]=="POST") {
    require 'connect.php';
    $user_ID = $_POST['id'];
    getSum($user_ID);
}

function getSum($id){
    global $connect;
    $fullExpenseAmountQuery = "SELECT SUM(amount) AS FullExpenseAmount FROM expense WHERE user_id='$id';";
    $fullExpenseAmountResult = mysqli_query($connect, $fullExpenseAmountQuery);


    $fullIncomeAmountQuery = "SELECT SUM(amount) AS FullIncomeAmount FROM income WHERE user_id='$id';";
    $fullIncomeAmountResult = mysqli_query($connect, $fullIncomeAmountQuery);

    $lastExpense = "SELECT notes AS 'last' FROM expense WHERE user_id='$id' ORDER BY id DESC LIMIT 1;";
    $lastExpenseResult = mysqli_query($connect, $lastExpense);

    $balanceProcedure = "CALL Balance('$id');";
    $balanceResult = mysqli_query($connect, $balanceProcedure);

    $temp_array = array();
    $temp_array[0] = mysqli_fetch_assoc($fullExpenseAmountResult);
    $temp_array[1] = mysqli_fetch_assoc($fullIncomeAmountResult);
    $temp_array[2] = mysqli_fetch_assoc($lastExpenseResult);
    $temp_array[3] = mysqli_fetch_assoc($balanceResult);



    header('Content-Type: application/json');
    echo json_encode(array("expensesInfo"=>$temp_array));
}